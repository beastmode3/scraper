#!/usr/bin/env python3
from best_buy_scraper import BestBuyScraper

airpods_url = "https://www.bestbuy.com/site/apple-airpods-pro-white/5706659.p?skuId=5706659"
rtx_gpu_url = "https://www.bestbuy.com/site/nvidia-geforce-rtx-3080-10gb-gddr6x-pci-" \
              "express-4-0-graphics-card-titanium-and-black/6429440.p?skuId=6429440"

scraper = BestBuyScraper(airpods_url)
print(f'Airpods costs: {scraper.get_price()}')
print(f'Availability: {scraper.get_stock()}')
scraper.cleanup()

scraper = BestBuyScraper(rtx_gpu_url)
print(f'RTX costs: {scraper.get_price()}')
print(f'Availability: {scraper.get_stock()}')
scraper.cleanup()
