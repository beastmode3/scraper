from enum import Enum, auto
from selenium import webdriver
from selenium.webdriver.firefox.options import Options


class Availability(Enum):
    IN_STOCK = auto()
    OUT_OF_STOCK = auto()


class BestBuyScraper:

    price_xpath = "//div[contains(@class,'priceView-customer-price')]/span[1]"
    cart_button_xpath = "//div[contains(@class,'add-to-cart-button')]"

    button_map = {
        "Sold Out": Availability.OUT_OF_STOCK,
        "Add to Cart": Availability.IN_STOCK
    }

    def __init__(self, url: str):
        options = Options()
        options.headless = True

        self.url = url
        self.driver = webdriver.Firefox(options=options)
        self.refresh()

    def refresh(self):
        self.driver.get(self.url)

    def get_price(self) -> str:
        price_elem = self.driver.find_element_by_xpath(self.price_xpath)
        return price_elem.text

    def get_stock(self) -> Availability:
        stock_elem = self.driver.find_element_by_xpath(self.cart_button_xpath)
        return self.button_map[stock_elem.text]

    def cleanup(self):
        self.driver.close()
