use std::collections::HashMap;

#[tokio::main]
async fn get() -> Result<(), Box<dyn std::error::Error>> {
    println!("Rust web page:");
    let resp = reqwest::get("https://www.rust-lang.org")
        .await?
        .text()
        .await?;
    println!("{:?}", resp);
    Ok(())
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    println!("IP: ");
    let resp = reqwest::blocking::get("https://httpbin.org/ip")?
        .json::<HashMap<String, String>>()?;
    println!("{:#?}", resp);

    get().expect("Failed to load page.");

    Ok(())
}

